const firebase = require('firebase');
const database = require('../../../db.js');

const db = database.db;
const auth = firebase.auth();

module.exports = {

  /* LOGIN */
  async login (req, res) {
    return firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
      .then((response) => {
        res.json(response);
        return response;
      }).catch(err => {
        res.json(err);
        return err;
      });
  },

  /* REGISTER */
  async register(req, res) {
    auth.createUserWithEmailAndPassword(req.body.email, req.body.password)
      .then((response) => {
        db.collection('users').doc(response.user.uid).set({
          email: req.body.email,
        })
          .catch((err) => {
            console.error(err);
            res.send(err);
          });
        res.json({ data: { uid: response.user.uid }, code: 'OK' });
      })
      .catch((err) => {
        console.error(err);
        res.send(err);
      });
  },

  /* LOGOUT */
  async logout (req, res) {
    return firebase.auth().signOut()
      .then(() => {
        const out = { data: 'User Logout', code: 'OK' };
        res.status(200).json(out);
        return out;
      }).catch(err => {
        res.json(err);
        return err;
      });
  },

}; // end of module.export