# CSUSM Project

Team:
* Ralph Lira
* Daniel Martinez
* Chihiro Nishijima
* Hugo Prat
* Hugo Janasik

Foobar is a Python library for dealing with word pluralization.

## Project
* Server (NodeJS)
* Mobile App (Flutter)

## License
[MIT](https://choosealicense.com/licenses/mit/)